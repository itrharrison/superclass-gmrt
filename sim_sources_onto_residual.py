import pdb
import numpy as np
from scipy import random
from astropy.io import fits
from astropy import units as uns
from matplotlib import pyplot as plt

import galsim # n.b. this should be installed and in the pythonpath!

rseed = 123456

data_path = '/local/scratch/harrison/superCLASS/gmrt/'

residual_image = fits.getdata('scg_pydsm.residual.fits')

# get pixel scale
header = fits.getheader(data_path+'scg_pydsm.residual.fits')
pixel_size_deg = abs(header['CDELT1'])*uns.degree
pixel_size = pixel_size_deg.to(uns.arcsec).value

# set up grid of sources
beam_size = 13/pixel_size
pixel_scale = 1
npix_x = residual_image.shape[1]
npix_y = residual_image.shape[0]
nx = 10
ny = 16

overlap_radius = 3*beam_size

flux_list = np.linspace(0.15,50,10)

#flux_list = np.array([100])

for flux in flux_list:
  run_name = 'flux_{0}'.format(flux)
  source_list = np.array([])
  id_list = np.array([])
  x_list = np.array([])
  y_list = np.array([])
  size_list = np.array([])
  rms_list = np.array([])

  source_id = 1

  image = galsim.Image(npix_x, npix_y, scale=pixel_scale)
  full_image = np.zeros_like(residual_image)
  im_centre = image.bounds.trueCenter()
  big_fft_params = galsim.GSParams(maximum_fft_size=12288)
  
  for ix in np.arange(nx):
    for iy in np.arange(ny):

      while True:
        xposn = np.random.uniform(xmin, xmax)
        x_idx = int(xposn + npix_x/2)
        yposn = np.random.uniform(ymin, ymax)
        y_idx = int(yposn + npix_y/2)

        distances = np.sqrt((xposn - x_list)**2. + (yposn - y_list)**2.)
        radius_test = np.sum(distances < overlap_radius)
        in_field_test = ~np.isnan(residual_image[xposn, yposn])
        if radius_test*in_field_test:
          break

      pos = galsim.PositionD(xposn, yposn)
      #snr = random.uniform(4,12)
      size = beam_size
      source = galsim.Gaussian(fwhm=size, flux=1,  gsparams=big_fft_params)
      source.drawImage(image=image, offset=pos, add_to_image=False)
      speak = image.array.max()
      mu = flux/speak
      full_image = full_image + mu*image.array

      source_list = np.append(source_list, source)

      id_list = np.append(id_list, source_id)
      x_list = np.append(x_list, x_idx[ix])
      y_list = np.append(y_list, y_idx[iy])
      snr_list = np.append(snr_list, snr)
      rms_list = np.append(rms_list, residual_image[y_idx[iy], x_idx[ix]])
      flux_list = np.append(flux_list, flux)
      size_list = np.append(size_list, size)
      print(snr, source_id)
      source_id += 1