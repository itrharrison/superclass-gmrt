'''
Open residual, rms images
Cut out central portion

Place 11.5 arcsec Gaussians on grid, with flux given by RMS map and sampled from
- Uniform distribution SNR 4-12
- True SNR distribution?

Also consider not grid but even sampling of values from RMS map
(i.e. different noise regimes in a fair/systematic way).
Make a histogram to look at this.
'''

import pdb
import numpy as np
from scipy import random
from astropy.io import fits
from astropy import units as uns
from matplotlib import pyplot as plt

import galsim # n.b. this should be installed and in the pythonpath!

rseed = 123456

data_path = '/local/scratch/harrison/superCLASS/gmrt/'

# open residual, rms images, cut out a rectangular portion
rms_image = fits.getdata(data_path+'IMAGES/scg_pydsm_3sig_5sig.rms.fits')
rms_image = rms_image[0,0]
#residual_image = fits.getdata(data_path+'SuperCLASS_GMRT_pybdsm.RESIDUAL.FITS')
#residual_image = residual_image[0,0]
#rms_image = rms_image[685:5050, 975:3150]
#residual_image = residual_image[685:5050, 975:3150]

# get pixel scale
header = fits.getheader(data_path+'IMAGES/scg_pydsm_3sig_5sig.rms.fits')
pixel_size_deg = abs(header['CDELT1'])*uns.degree
pixel_size = pixel_size_deg.to(uns.arcsec).value

# set up grid of sources
beam_size = 13/pixel_size
pixel_scale = 1
npix_x = rms_image.shape[1]
npix_y = rms_image.shape[0]
nx = 25
ny = 40

#flux_ev = galsim.DistDeviate(rseed, 'Speak_dist.txt')
#size_ev = galsim.DistDeviate(rseed, 'maj_dist.txt')

xposns = 0.9*np.linspace(-npix_x/2, npix_x/2, nx)
x_idx = np.asarray(xposns + npix_x/2, dtype=int)
yposns = 0.9*np.linspace(-npix_y/2, npix_y/2, ny)
y_idx = np.asarray(yposns + npix_y/2, dtype=int)



snr_list = np.arange(4,13)

#snr_list = np.array([100])

for snr in snr_list:
  run_name = 'snr_{0}'.format(snr)
  source_list = np.array([])
  id_list = np.array([])
  x_list = np.array([])
  y_list = np.array([])
  snr_list= np.array([])
  flux_list = np.array([])
  size_list = np.array([])
  rms_list = np.array([])

  source_id = 1

  image = galsim.Image(npix_x, npix_y, scale=pixel_scale)
  full_image = np.zeros_like(rms_image)
  im_centre = image.bounds.trueCenter()
  big_fft_params = galsim.GSParams(maximum_fft_size=12288)
  
  for ix in np.arange(nx):
    for iy in np.arange(ny):

      pos = galsim.PositionD(xposns[ix], yposns[iy])
      #snr = random.uniform(4,12)
      flux = snr*rms_image[y_idx[iy], x_idx[ix]]
      if np.isnan(flux):
        continue
      size = beam_size
      source = galsim.Gaussian(fwhm=size, flux=1,  gsparams=big_fft_params)
      source.drawImage(image=image, offset=pos, add_to_image=False)
      speak = image.array.max()
      mu = flux/speak
      full_image = full_image + mu*image.array

      source_list = np.append(source_list, source)

      id_list = np.append(id_list, source_id)
      x_list = np.append(x_list, x_idx[ix])
      y_list = np.append(y_list, y_idx[iy])
      snr_list = np.append(snr_list, snr)
      rms_list = np.append(rms_list, rms_image[y_idx[iy], x_idx[ix]])
      flux_list = np.append(flux_list, flux)
      size_list = np.append(size_list, size)
      print(snr, source_id)
      source_id += 1


  catalogue = np.column_stack([id_list,
                               x_list,
                               y_list,
                               snr_list,
                               rms_list,
                               flux_list,
                               size_list*pixel_size
                               ])
  
  np.savetxt(data_path+'{0}_catalogue.txt'.format(run_name), catalogue)

  sources_fname = data_path+'{0}_sources.fits'.format(run_name)
  sim_image_fname = data_path+'{0}_image.fits'.format(run_name)

  hdu = fits.PrimaryHDU(full_image, header=header)
  hdulist = fits.HDUList([hdu])
  hdulist.writeto(sources_fname, clobber=True)
  '''
  sources_image = full_image
  sim_image = sources_image + residual_image

  hdu = fits.PrimaryHDU(sim_image, header=header)
  hdulist = fits.HDUList([hdu])
  hdulist.writeto(sim_image_fname, clobber=True)

  fig, axes = plt.subplots(1, 4, subplot_kw={'xticks': [], 'yticks': []})

  axes[0].imshow(sources_image)
  axes[0].set_title('Sources')
  axes[1].imshow(rms_image)
  axes[1].set_title('RMS')
  axes[2].imshow(residual_image)
  axes[2].set_title('Residual')
  axes[3].imshow(sim_image)
  axes[3].set_title('Sources + Residual')

  plt.savefig('sources-noise-sim-{0}.png'.format(run_name), bbox_inches='tight')
  '''
